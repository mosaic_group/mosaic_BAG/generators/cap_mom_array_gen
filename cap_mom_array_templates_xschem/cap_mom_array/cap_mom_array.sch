v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 210 -330 210 -290 {
lab=plus}
N 270 -240 300 -240 {
lab=VDD}
N 210 -190 210 -160 {
lab=minus}
N 210 -130 210 -100 {
lab=VDD}
N 270 -50 300 -50 {
lab=VDD}
N 210 0 210 20 {
lab=VSS}
C {devices/iopin.sym} 80 -240 2 0 {name=p2 lab=plus}
C {devices/iopin.sym} 80 -270 2 0 {name=p3 lab=VSS}
C {devices/lab_pin.sym} 210 -330 2 0 {name=l4 sig_type=std_logic lab=plus}
C {devices/iopin.sym} 80 -220 2 0 {name=p1 lab=minus}
C {devices/lab_pin.sym} 210 -160 2 0 {name=l1 sig_type=std_logic lab=minus}
C {devices/lab_pin.sym} 210 -130 2 0 {name=l7 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 210 20 2 0 {name=l8 sig_type=std_logic lab=VSS}
C {devices/iopin.sym} 80 -290 2 0 {name=p4 lab=VDD}
C {cap_mom_gen/cap_mom_templates_xschem/cap_mom/cap_mom.sym} 140 -170 0 0 {name=XCAP
spiceprefix=X
}
C {cap_mom_gen/cap_mom_templates_xschem/cap_mom/cap_mom.sym} 140 20 0 0 {name=XDUM
spiceprefix=X
}
C {devices/lab_pin.sym} 300 -240 2 0 {name=l2 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 300 -50 2 0 {name=l3 sig_type=std_logic lab=VDD}
