#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *

from cap_mom_gen.params import cap_mom_layout_params


@dataclass
class cap_mom_array_layout_params(LayoutParamsBase):
    """
    Parameter class for cap_mom_array_gen

    Args:
    ----

    cap_params: cap_mom_layout_params
        Parameters for cap_mom sub-generators

    nx: int
        number of capacitors in a row

    ny: int
        number of capacitors in a column

    ndum: int
        number of dummies

    sub_lch: Union[float, int]
        Substrate channel length.

    sub_w: Union[float, int]
        Substrate width.

    sub_type: str
        Substrate type.  Either "ptap" or "ntap".

    sub_threshold: str
        Substrate threshold.

    sub_tr_w: Optional[int]
        substrate track width in number of tracks.  None for default.

    show_pins : bool
        True to create pin labels
    """

    cap_params: cap_mom_layout_params
    nx: int
    ny: int
    ndum: int
    sub_lch: Union[float, int]
    sub_w: Union[float, int]
    sub_type: str
    sub_threshold: str
    sub_tr_w: Optional[int]
    show_pins: bool

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> cap_mom_array_layout_params:
        return cap_mom_array_layout_params(
            cap_params=cap_mom_layout_params.finfet_defaults(min_lch),
            nx=8,
            ny=4,
            ndum=1,
            sub_lch=3,
            sub_w=17,
            sub_type='ntap',
            sub_threshold='standard',
            sub_tr_w=1,
            show_pins=True,
        )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> cap_mom_array_layout_params:
        return cap_mom_array_layout_params(
            cap_params=cap_mom_layout_params.planar_defaults(min_lch),
            nx=8,
            ny=4,
            ndum=1,
            sub_lch=3 * min_lch,
            sub_w=17 * min_lch,  # NOTE: 5.0e-7 would be 16.66666
            sub_type='ntap',
            sub_threshold='standard',
            sub_tr_w=4,
            show_pins=True,
        )


@dataclass
class cap_mom_array_params(GeneratorParamsBase):
    layout_parameters: cap_mom_array_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> cap_mom_array_params:
        return cap_mom_array_params(
            layout_parameters=cap_mom_array_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[]
        )
