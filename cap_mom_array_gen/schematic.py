import os
from typing import Dict

from bag.design import Module

yaml_file = os.path.join(f'{os.environ["BAG_GENERATOR_ROOT"]}/BagModules/cap_mom_array_templates',
                         'netlist_info', 'cap_mom_array.yaml')


# noinspection PyPep8Naming
class schematic(Module):
    """Module for library cap_mom_array_templates cell cap_mom_array.

    Fill in high level description here.
    """

    def __init__(self, bag_config, parent=None, prj=None, **kwargs):
        super().__init__(bag_config, yaml_file, parent=parent, prj=prj, **kwargs)

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """Returns a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : Optional[Dict[str, str]]
            dictionary from parameter names to descriptions.
        """
        return dict(
            cap_sch_params='capacitor schematic parameters',
            nx='number of capacitors in a row',
            ny='number of capacitors in a column',
            ndum='number of dummy capacitors',
        )

    def design(self,
               cap_sch_params: Dict,
               nx: int,
               ny: int,
               ndum: int):
        """To be overridden by subclasses to design this module.

        This method should fill in values for all parameters in
        self.parameters.  To design instances of this module, you can
        call their design() method or any other ways you coded.

        To modify schematic structure, call:

        rename_pin()
        delete_instance()
        replace_instance_master()
        reconnect_instance_terminal()
        restore_instance()
        array_instance()
        """
        num_res_series = (nx - 2) * (ny - 2)
        num_res_dummies = (nx + ny) * 2 - 4

        name_list_dum = ['XDM_' + str(x) for x in range(0, num_res_dummies)]
        name_list_top = ['XCP_' + str(x) for x in range(0, num_res_series)]

        term_list_dum = []
        for idx in range(num_res_dummies):
            term_dict_dum = {
                'plus': 'VDD',
                'minus': 'VSS',
                'VDD': 'VDD'
            }
            term_list_dum.append(term_dict_dum)

        term_list_top = []
        for idx in range(num_res_series):
            term_dict_top = {
                'plus': 'plus',
                'minus': 'minus',
                'VDD': 'VDD'
            }
            term_list_top.append(term_dict_top)

        self.instances['XCAP'].design(**cap_sch_params)
        self.instances['XDUM'].design(**cap_sch_params)

        self.array_instance('XDUM', name_list_dum, term_list_dum)
        self.array_instance('XCAP', name_list_top, term_list_top)
