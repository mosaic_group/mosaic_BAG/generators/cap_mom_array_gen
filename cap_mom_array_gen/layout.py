import copy
from typing import *

from bag.layout.routing import TrackID
from bag.layout.template import TemplateBase, TemplateType
from bag.layout.util import BBox
from abs_templates_ec.analog_core import AnalogBase

from sal.routing_grid import RoutingGridHelper
from sal.via import ViaDirection

from .capacitor.substrate import SubstrateWrapper
from .params import cap_mom_array_layout_params
from cap_mom_gen.layout import cap_mom


class unwrapped_core_layout(AnalogBase):
    """An array of MOM caps.
    ports are drawn on the layer above cap_top_layer.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template_feifei.TemplateDB`
        the template database.
    lib_name : str
        the layout library name.
    params : dict[str, any]
        the parameter values.
    used_names : set[str]
        a set of already used cell names.
    **kwargs :
        dictionary of optional parameters.  See documentation of
        :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        # type: (TemplateDB, str, Dict[str, Any], Set[str], **kwargs) -> None
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        return dict(
            params='cap_mom_array_layout_params parameters object',
        )

    def draw_layout(self):
        array_params: cap_mom_array_layout_params = self.params['params']
        params = array_params.cap_params

        res = self.grid.resolution

        # Define pitches of layers
        io_layer = params.top_layer
        vm_layer = 5
        pitch_bot = self.grid.get_track_pitch(layer_id=params.bot_layer, unit_mode=True)
        pitch_vm = self.grid.get_track_pitch(layer_id=vm_layer, unit_mode=True)
        pitch_hm = self.grid.get_track_pitch(layer_id=vm_layer+1, unit_mode=True)
        min_len = self.grid.get_track_width(layer_id=vm_layer, width_ntr=1, unit_mode=True)

        # Define metal layers to use for connecting capacitors (using layer id 4 and 5)
        cap_conn_bot_layer = self.grid.tech_info.get_layer_name(4)
        cap_conn_top_layer = self.grid.tech_info.get_layer_name(5)
        cap_conn_bot_layer_type = self.grid.tech_info.get_layer_type(cap_conn_bot_layer)

        # Defining the minimum dimensions of layer below vm_layer(layer 3 of the stack)
        width_lay4 = self.grid.get_track_width(layer_id=vm_layer-1, width_ntr=1, unit_mode=True)
        pitch_lay4 = self.grid.get_track_pitch(layer_id=vm_layer-1, unit_mode=True)
        min_len_lay4 = self.grid.tech_info.get_min_length_unit(layer_type=cap_conn_bot_layer_type, w_unit=width_lay4)

        # Define space between single MOMcaps in array
        cap_pitch_x = pitch_vm
        dy = pitch_lay4
        w_core = params.width + 2 * pitch_vm - 2 * pitch_bot
        h_core = params.height + 3 * pitch_lay4
        w_edge = 0
        h_edge = 0
        ny = array_params.ny + 2 * array_params.ndum
        nx = array_params.nx + 2 * array_params.ndum

        # Logic to set 'w_core' as smallest multiple of 'cap_pitch_x' larger than value of 'w_core',
        # so that ports of the capacitor can be transformed with out any error
        w_core = -(-w_core // cap_pitch_x) * cap_pitch_x
        h_core = -(-h_core // pitch_lay4) * pitch_lay4

        # NOTE (mjk):
        #     minimum parameter values nx/ny is 3/3, so we're sure the plus/minus pins are drawn
        #     in order to make space for the plus/minus pins, we'll add a padding instead of starting at 0

        left_padding = w_core
        pin_width = w_core + w_core//2

        dx = left_padding + cap_pitch_x

        grid_helper = RoutingGridHelper(template_base=self,
                                        unit_mode=True,
                                        layer_id=5,  # NOT used
                                        track_width=1)   # NOT used

        # Add instance and connect MOMcaps in array, one row and one column in each side is dummy capacitor
        core_master = self.new_template(temp_cls=cap_mom, params=dict(params=params.copy()))
        for row in range(ny):
            for col in range(nx):
                cur_name = 'XCORE%d' % (col + nx * row)
                cur_loc = (dx + w_edge + col * w_core,
                           dy + h_edge + row * h_core)
                core_inst = self.add_instance(master=core_master, inst_name=cur_name, loc=cur_loc, unit_mode=True)

                # Correction factor after considering offset created to align with blk_pitch of metal layers in X-axis
                w_e = (core_inst.bound_box.width_unit - params.width) // 2

                grid_helper.extend_pin(warr_id=core_inst.get_all_port_pins('plus')[0],
                                       via_dir=ViaDirection.BOTTOM,
                                       wire_layer_id=5)
                grid_helper.extend_pin(warr_id=core_inst.get_all_port_pins('minus')[0],
                                       via_dir=ViaDirection.TOP,
                                       wire_layer_id=5)
                if row == 0 and col == 0:
                    grid_helper.connect_level_down(warr_id=core_inst.get_all_port_pins('plus')[0],
                                                   via_dir=ViaDirection.BOTTOM,
                                                   step=0.0)
                if row == ny - 1 and col == 0:
                    grid_helper.connect_level_down(warr_id=core_inst.get_all_port_pins('minus')[0],
                                                   via_dir=ViaDirection.TOP,
                                                   step=0.0)

                if row != 0 and row != ny - 1 and col != 0 and col != nx-1:  # row != 0 and row != ny - 1
                    grid_helper.connect_level_down(warr_id=core_inst.get_all_port_pins('plus')[0],
                                                   via_dir=ViaDirection.BOTTOM,
                                                   step=0.0)
                    grid_helper.connect_level_down(warr_id=core_inst.get_all_port_pins('minus')[0],
                                                   via_dir=ViaDirection.TOP,
                                                   step=0.0)

        for row in range(ny):
            # Connect Dummy capacitors using add_rect on M5
            if row == 0 or row == ny - 1:
                # Using default parameters the convention is :
                #            Upper port/terminal of capacitor -> MINUS/negative
                #            Lower port/terminal of capacitor -> PLUS/positive
                
                # Horizontal connections of positive terminals of capacitors in first and last row
                box = BBox(left=dx + w_e - pitch_vm,
                           bottom=dy + h_edge + row * h_core - 1.5 * min_len,
                           right=dx + w_e + nx * w_core,
                           top=dy + h_edge + row * h_core - 0.5 * min_len,
                           resolution=res,
                           unit_mode=True)
                self.add_rect(cap_conn_top_layer, box)
                
                #  Horizontal connnections of negative terminals of capacitors in first and last row
                box = BBox(left=dx + w_e,
                           bottom=dy + h_edge + row * h_core + pitch_vm//2 + params.height + 0.5 * min_len,
                           right=dx + w_e + nx * w_core + pitch_vm,
                           top=dy + h_edge + row * h_core + pitch_vm//2 + params.height + 1.5 * min_len,
                           resolution=res,
                           unit_mode=True)
                self.add_rect(cap_conn_top_layer, box)
                
                # Vertical connection of positive terminal of first and last row, on the left-most end of array
                box = BBox(left=dx + w_e - pitch_vm,
                           bottom=dy + h_edge - 1.5 * min_len,
                           right=dx + w_e - pitch_vm + min_len,
                           top=dy + h_edge + row * h_core + params.height + 1.5 * min_len,
                           resolution=res,
                           unit_mode=True)
                self.add_rect(cap_conn_top_layer, box)

                # Vertical connections of negative terminal of first and last row, on the right-most end of array
                box = BBox(left=dx + w_e + nx * w_core + pitch_vm - min_len,
                           bottom=dy + h_edge - 1.5 * min_len,
                           right=dx + w_e + nx * w_core + pitch_vm,
                           top=dy + h_edge + row * h_core + params.height + 1.5 * min_len,
                           resolution=res,
                           unit_mode=True)
                self.add_rect(cap_conn_top_layer, box)

                # Vertical connection of negative terminals for left column of dummy transistors.
                box = BBox(left=dx + w_e + w_core - min_len,
                           bottom=dy + h_edge +  pitch_vm + params.height,
                           right=dx + w_e + w_core,
                           top=dy + h_edge + (ny - 2) * h_core +  pitch_vm + params.height + min_len,
                           resolution=res,
                           unit_mode=True)
                self.add_rect(cap_conn_top_layer, box)
                
                # Vertical connection of positive terminals of right column of dummy transistors
                box = BBox(left=dx + w_e + (nx - 1) * w_core - min_len,
                           bottom=dy + h_edge + h_core - 1.5 * min_len,
                           right=dx + w_e + (nx - 1) * w_core,
                           top=dy + h_edge + (ny - 1) * h_core,
                           resolution=res,
                           unit_mode=True)
                self.add_rect(cap_conn_top_layer, box)
                
                # Connect main capacitors
            else:
                ## Left dummies horizontal connection to PLUS terminal
                box = BBox(left=dx + w_e - pitch_vm,
                           bottom=dy + h_edge + row * h_core - 1.5 * min_len,
                           right=dx + w_e + w_core - pitch_vm,
                           top=dy + h_edge + row * h_core - min_len // 2,
                           resolution=res,
                           unit_mode=True)
                self.add_rect(cap_conn_top_layer, box)
                ## Left dummies horizontal connection to MINUS terminal
                box = BBox(left=dx + w_e,
                           bottom=dy + h_edge + row * h_core + pitch_vm//2 + params.height + 0.5 * min_len,
                           right=dx + w_e + w_core,
                           top=dy + h_edge + row * h_core + pitch_vm//2 + params.height + 1.5 * min_len,
                           resolution=res,
                           unit_mode=True)
                self.add_rect(cap_conn_top_layer, box)

                ##  Right dummies horizontal connection to PLUS terminal
                box = BBox(left=dx + w_e + (nx - 1) * w_core,
                           bottom=dy + h_edge + row * h_core - 1.5 * min_len,
                           right=dx + w_e + nx * w_core,
                           top=dy + h_edge + row * h_core - min_len // 2,
                           resolution=res,
                           unit_mode=True)
                self.add_rect(cap_conn_top_layer, box)
                
                ##  Right dummies horizontal connection to MINUS terminal
                box = BBox(left=dx + w_e + (nx - 1) * w_core + pitch_vm,
                           bottom=dy + h_edge + row * h_core + pitch_vm//2 + params.height + 0.5 * min_len,
                           right=dx + w_e + nx * w_core + pitch_vm,
                           top=dy + h_edge + row * h_core + pitch_vm//2 + params.height + 1.5 * min_len,
                           resolution=res,
                           unit_mode=True)
                self.add_rect(cap_conn_top_layer, box)
                
                ## Horizontal connection of PLUS terminals of main capacitors
                box1 = BBox(left=dx + w_e + w_core + pitch_vm,
                            bottom=dy + h_edge + row * h_core - 1.5 * min_len,
                            right=dx + w_e + (nx-1) * w_core - pitch_vm,
                            top=dy + h_edge + row * h_core - 0.5 * min_len,
                            resolution=res,
                            unit_mode=True)
                self.add_rect(cap_conn_bot_layer, box1)

                ## Horizontal connection of MINUS terminals of main capacitors
                box2 = BBox(left=dx + w_e + w_core + pitch_vm,
                            bottom=dy + h_edge + row * h_core + pitch_vm//2 + params.height + 0.5 * min_len,
                            right=dx + w_e + (nx-1) * w_core - pitch_vm,
                            top=dy + h_edge + row * h_core + pitch_vm//2  + params.height + 1.5 * min_len,
                            resolution=res,
                            unit_mode=True)
                self.add_rect(cap_conn_bot_layer, box2)

                vm_width = 1

                # Vertical connections to PLUS and MINUS terminals of main capacitors
                for col in range(2, nx - 1):
                    grid = self.grid
                    res = grid.resolution
                    vr_id = self.grid.coord_to_nearest_track(
                        layer_id=vm_layer,
                        coord=dx + w_e + col * w_core - (pitch_vm - pitch_bot // 2) // 2,
                        mode=0,
                        half_track=True,
                        unit_mode=True)
                    vr_tid = TrackID(layer_id=vm_layer, track_idx=vr_id, width=vm_width)
                    self.add_wires(layer_id=vm_layer,
                                   track_idx=vr_tid.base_index,
                                   lower=dy + h_edge + h_core - pitch_lay4,
                                   upper=dy + h_edge + (ny - 2) * h_core + params.height + pitch_lay4,
                                   width=1,
                                   unit_mode=True)
                    if col % 2 == 0:
                        warr = self.connect_bbox_to_tracks(cap_conn_bot_layer, box1, vr_tid)
                    else:
                        warr = self.connect_bbox_to_tracks(cap_conn_bot_layer, box2, vr_tid)

                # Set tracks for pins and add pins
                vr_id = self.grid.coord_to_nearest_track(
                    layer_id=vm_layer - 1,
                    coord=dy + h_edge + h_core - 0.5 * min_len,
                    mode=-1,
                    half_track=True,
                    unit_mode=True)
                vr_tid = TrackID(vm_layer - 1, vr_id, width=vm_width)
                warr = self.add_wires(layer_id=vm_layer - 1,
                                      track_idx=vr_tid.base_index,
                                      lower=0,
                                      upper=pin_width,
                                      width=1,
                                      unit_mode=True)
                self.add_pin('plus', warr, show=params.show_pins)
                self.extend_wires(warr, upper=dx + w_e + w_core + pitch_vm + pin_width, unit_mode = True)

                vr_id = self.grid.coord_to_nearest_track(
                    layer_id=vm_layer - 1,
                    coord=dy + h_edge + (ny - 2) * h_core + pitch_vm//2  + params.height + 0.5 * min_len,
                    mode=1,
                    half_track=True,
                    unit_mode=True)
                vr_tid = TrackID(layer_id=vm_layer - 1, track_idx=vr_id, width=vm_width)
                warr = self.add_wires(layer_id=vm_layer - 1,
                                      track_idx=vr_tid.base_index,
                                      lower=0,
                                      upper=pin_width,
                                      width=1,
                                      unit_mode=True)
                self.add_pin('minus', warr, show=params.show_pins)
                self.extend_wires(warr, upper=dx + w_e + w_core + pitch_vm + pin_width, unit_mode = True)

                vr_id = self.grid.coord_to_nearest_track(
                    layer_id=vm_layer - 1,
                    coord=dy + h_edge - min_len,
                    mode=0,
                    half_track=True,
                    unit_mode=True)
                vr_tid = TrackID(layer_id=vm_layer - 1, track_idx=vr_id, width=vm_width)
                warr = self.add_wires(layer_id=vm_layer - 1,
                                      track_idx=vr_tid.base_index,
                                      lower= w_e - 0.5*min_len_lay4,
                                      upper= w_e + 0.5*min_len_lay4,
                                      width=1,
                                      unit_mode=True)
                self.add_pin('VDD', warr, show=params.show_pins)
                self.extend_wires(warr, upper=dx + w_e + w_core, unit_mode = True)

                vr_id = self.grid.coord_to_nearest_track(
                    layer_id=vm_layer - 1,
                    coord=dy + h_edge + (ny - 1) * h_core + params.height + min_len,
                    mode=0,
                    half_track=True,
                    unit_mode=True)
                vr_tid = TrackID(layer_id=vm_layer - 1, track_idx=vr_id+0.5, width=vm_width)
                warr = self.add_wires(layer_id=vm_layer - 1,
                                      track_idx=vr_tid.base_index,
                                      lower=w_e - 0.5*min_len_lay4,
                                      upper=w_e + 0.5*min_len_lay4,
                                      width=1,
                                      unit_mode=True)
                self.add_pin('VSS', warr, show=params.show_pins)
                self.extend_wires(warr, upper=dx + w_e + w_core, unit_mode = True)

        # set size
        w_tot = 3*left_padding + w_core * nx + 2 * cap_pitch_x

        # # NOTE (mjk):
        # #     minimum parameter values nx/ny is 3/3,
        # #     so we can be sure that -w_core will be used drawing the pins
        # w_tot += w_core

        h_tot = h_core * ny + 2 * dy
        self.array_box = bnd_box = BBox(left=0, bottom=0, right=w_tot, top=h_tot,
                                        resolution=res, unit_mode=True)
        self.set_size_from_bound_box(io_layer, bnd_box, round_up=True)
        self.add_cell_boundary(bnd_box)

        self._sch_params = dict(
            cap_sch_params=core_master.sch_params.copy(),  # Get parameters from core_master to prevent repeat
            nx=nx,
            ny=ny,
            ndum=array_params.ndum,
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class substrate_wrapped_layout(SubstrateWrapper):
    """A MOM Cap with substrate contact.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None
    
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='cap_mom_array_layout_params parameters object',
        )

    def new_wrapped_master_template(self,
                                    disable_show_pins: bool,
                                    disable_half_blk_x: bool) -> TemplateType:
        params: cap_mom_array_layout_params = self.params['params'].copy()
        if disable_show_pins:
            params.cap_params.show_pins = False
        if disable_half_blk_x:
            params.cap_params.half_blk_x = False

        master = self.new_template(temp_cls=unwrapped_core_layout,
                                   params=dict(params=params))
        return master

    def draw_layout(self):
        """Draw the layout."""

        params: cap_mom_array_layout_params = self.params['params']

        if params.nx < 3:
            raise ValueError("minimum value for nx parameter: 3")
        if params.ny < 3:
            raise ValueError("minimum value for ny parameter: 3")

        self.draw_layout_helper(sub_lch=params.sub_lch,
                                sub_w=params.sub_w,
                                sub_tr_w=params.sub_tr_w,
                                sub_type=params.sub_type,
                                threshold=params.sub_threshold,
                                show_pins=params.show_pins,
                                is_passive=False,
                                bot_only=False)

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class cap_mom_array(substrate_wrapped_layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
